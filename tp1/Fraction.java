package tp1;

public class Fraction {
	private int numerateur;
	private int denumerateur;

	public Fraction() {

	}

	public Fraction(int numerateur, int denumerateur) {
		setNumerateur(numerateur);
		setDenumerateur(denumerateur);

	}

	public String toString() {
		return this.numerateur +"/" + this.denumerateur+"\nLe resultat = "+this.numerateur/this.denumerateur;
	}

	public int getNumerateur() {
		return numerateur;
	}

	public void setNumerateur(int numerateur) {
		this.numerateur = numerateur;
	}

	public int getDenumerateur() {
		return denumerateur;
	}

	public void setDenumerateur(int denumerateur) {
		this.denumerateur = denumerateur;
	}

}
